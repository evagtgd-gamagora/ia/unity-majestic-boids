﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFPS : MonoBehaviour {

    public BoidsController boidsController;

    private Boid boidFPS;
    private Rigidbody rbBoidFPS;

    void Awake () {
        GameObject bc = GameObject.FindGameObjectWithTag("GameController");
        boidsController = bc.GetComponent<BoidsController>();
    }
	
	void Update () {
        if(boidFPS != null)
        {
            if(rbBoidFPS == null)
                rbBoidFPS = boidFPS.GetComponent<Rigidbody>();

            transform.position = boidFPS.transform.position;
            Vector3 rotation = rbBoidFPS.velocity;
            transform.rotation = Quaternion.FromToRotation(Vector3.forward, rotation);
        }
        else
        {
            boidFPS = boidsController.getFirstBoid();
        }
    }
}
