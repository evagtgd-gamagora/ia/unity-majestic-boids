﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoidsController : MonoBehaviour {

    public GameObject border;
    public Boid boidPrefab;

    [HideInInspector] public int numbersOfBoids = 50;
    public Slider sNumberOfBoids;

    [HideInInspector] public float maximalVelocity = 35;
    public Slider sMaximalVelocity;
    [HideInInspector] public float minimalDistance = 10;
    public Slider sMinimalDistance;
    [HideInInspector] public float scopeDistance = 50;
    public Slider sScopeDistance;

    [HideInInspector] public float gatheringFactor = 130;
    public Slider sGatheringFactor;
    [HideInInspector] public float cohesionFactor = 40;
    public Slider sCohesionFactor;
    [HideInInspector] public float repulsionFactor = 22;
    public Slider sRepulsionFactor;
    [HideInInspector] public float targetFactor = 50;
    public Slider sTargetFactor;

    public float reboundFactorMin = 0.5f;
    public float reboundFactorMax = 2;
    public float reboundDistance = 5;

    public float minX, maxX, minY, maxY, minZ, maxZ;

    List<Boid> boids;
    bool started = false;

	// Use this for initialization
	void Start () {

        boids = new List<Boid>();
        //Calculating borders
        minX = border.transform.position.x - border.transform.localScale.x / 2;
        maxX = border.transform.position.x + border.transform.localScale.x / 2;
        minY = border.transform.position.y - border.transform.localScale.y / 2;
        maxY = border.transform.position.y + border.transform.localScale.y / 2;
        minZ = border.transform.position.z - border.transform.localScale.z / 2;
        maxZ = border.transform.position.z + border.transform.localScale.z / 2;
	}
	
	// Update is called once per frame
	void Update () {
        if(started)
        {
            UpdateValuesFromSliders();
            foreach (Boid boid in boids)
            {
                List<Boid> closeBoids = new List<Boid>();
                foreach (Boid otherBoid in boids)
                {
                    if (otherBoid == boid) continue;
                    if (boid.Distance(otherBoid) < scopeDistance)
                        closeBoids.Add(otherBoid);
                }

                boid.MoveCloserToTarget();
                boid.MoveCloser(boids);
                boid.MoveWith(boids);
                boid.MoveAway(boids);
            }
        }
	}

    public void InitBoids()
    {
        //Create boids at random positions
        for (int i = 0; i < numbersOfBoids; ++i)
        {
            CreateBoid();
        }

        CreateBoid();
        CreateBoid();
        started = true;
    }

    void UpdateValuesFromSliders()
    {
        numbersOfBoids = (int)sNumberOfBoids.value;

        maximalVelocity = sMaximalVelocity.value;
        minimalDistance = sMinimalDistance.value;
        scopeDistance = sScopeDistance.value;

        gatheringFactor = sGatheringFactor.value;
        cohesionFactor = sCohesionFactor.value;
        repulsionFactor = sRepulsionFactor.value;
        targetFactor = sTargetFactor.value;

        UpdateBoidsList();
    }

    void UpdateBoidsList()
    {
        int numDiff = boids.Count - numbersOfBoids;

        //If too much boids - Destroy
        for (int i = 0; i < numDiff; ++i)
            DestroyLastBoid();

        //If not enough boids - Create
        for (int i = 0; i < -numDiff; ++i)
            CreateBoid();
    }

    void CreateBoid()
    {
        Vector3 randomPosition = new Vector3(
                Random.Range(minX, maxX),
                Random.Range(minY, maxY),
                Random.Range(minZ, maxZ)
            );
        Boid boid = Instantiate(boidPrefab);
        boid.transform.position = randomPosition;
        boids.Add(boid);
    }

    void DestroyLastBoid()
    {
        int last = boids.Count - 1;
        Boid boid = boids[last];
        boids.RemoveAt(last);
        Destroy(boid.gameObject);
    }

    public Boid getFirstBoid()
    {
        if (boids.Count > 0)
            return boids[0];
        else
            return null;
    }
}
