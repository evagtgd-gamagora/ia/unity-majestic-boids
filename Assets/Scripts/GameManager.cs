﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public AudioSource titleAudio;
    public AudioSource gameAudio;
    public GameObject titleScreen;

    BoidsController boidsController;

    bool started = false;
    

	// Use this for initialization
	void Start () {  
        boidsController = GetComponent<BoidsController>();
        titleAudio.Play();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (!started && Input.GetKeyDown(KeyCode.Space))
        {
            started = true;
            titleAudio.Stop();
            gameAudio.Play();
            Destroy(titleScreen);
            InitGame();
        }
    }
    
    void InitGame()
    {
        boidsController.InitBoids();
    }
}
