﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LollipopController : MonoBehaviour {

    BoidsController boidsController;

    // Use this for initialization
    void Start () {
        GameObject bc = GameObject.FindGameObjectWithTag("GameController");
        boidsController = bc.GetComponent<BoidsController>();
    }
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * 100.0f;
        float y = Input.GetAxis("Vertical") * Time.deltaTime * 100.0f;
        float z = 0;

        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            z += Time.deltaTime * 100.0f;
        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            z -= Time.deltaTime * 100.0f;


        //transform.Rotate(0, x, 0);
        transform.Translate(x, y, z);

        Vector3 clampedPosition = new Vector3();
        clampedPosition.x = Mathf.Clamp(transform.position.x, boidsController.minX, boidsController.maxX);
        clampedPosition.y = Mathf.Clamp(transform.position.y, boidsController.minY, boidsController.maxY);
        clampedPosition.z = Mathf.Clamp(transform.position.z, boidsController.minZ, boidsController.maxZ);

        transform.position = clampedPosition;
    }
}
